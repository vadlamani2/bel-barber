import { Component, OnInit } from '@angular/core';
import { HomePageImagesService } from 'src/app/services/home-page-images.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  public homeImg:any = []
  constructor(private _homeImages: HomePageImagesService) { }

  ngOnInit(){
    this._homeImages.getBannerImg()
    .subscribe(res => this.homeImg = res, err => console.log("Error while receiving Data from service" + err))
  }

}
