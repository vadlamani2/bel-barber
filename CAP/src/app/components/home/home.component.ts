import { Component, OnInit } from '@angular/core';
import { HomePageImagesService } from 'src/app/services/home-page-images.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public homeImg:any = []
  constructor(private _homeImages: HomePageImagesService) { }

  ngOnInit(){
    this._homeImages.getBannerImg()
    .subscribe(res => this.homeImg = res, err => console.log("Error while receiving Data from service" + err))
  }

}
