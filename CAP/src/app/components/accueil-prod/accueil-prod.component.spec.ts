import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccueilProdComponent } from './accueil-prod.component';

describe('AccueilProdComponent', () => {
  let component: AccueilProdComponent;
  let fixture: ComponentFixture<AccueilProdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccueilProdComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AccueilProdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
