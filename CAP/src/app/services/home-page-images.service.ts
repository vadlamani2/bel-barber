import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { homeImages } from '../homeImage';

@Injectable({
  providedIn: 'root'
})
export class HomePageImagesService {
  private imgURL = 'assets/data/imagePath.json'
  constructor(private _http: HttpClient) { }
  getBannerImg():Observable<homeImages[]>{
    return this._http.get<homeImages[]>(this.imgURL)  
  }
}
