import { TestBed } from '@angular/core/testing';

import { HomePageImagesService } from './home-page-images.service';

describe('HomePageImagesService', () => {
  let service: HomePageImagesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HomePageImagesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
